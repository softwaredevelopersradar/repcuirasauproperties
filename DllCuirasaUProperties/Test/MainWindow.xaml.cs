﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //prop.SetPassword("123");
        }

        private void Prop_OnLocalDefaultButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("LocalDefault!");
        }

        private void Prop_OnGlobalDefaultButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("GlobalDefault!");

        }

        private void Prop_OnLanguageChanged(object sender, DllCuirasaUProperties.Models.Languages e)
        {
            MessageBox.Show(e.ToString());
        }
    }
}
