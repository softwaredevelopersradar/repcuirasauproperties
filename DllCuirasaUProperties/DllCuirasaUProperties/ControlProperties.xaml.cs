﻿using DllCuirasaUProperties.Models;
using DllCuirasaUProperties.Models.Global;
using DllCuirasaUProperties.Models.Local;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using WpfPasswordControlLibrary;
using Orientation = DllCuirasaUProperties.Models.Global.Orientation;

namespace DllCuirasaUProperties
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ControlProperties : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<LocalProperties> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler<GlobalProperties> OnGlobalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler OnGlobalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<Languages> OnLanguageChanged = (sender, language) => { };
        public event EventHandler<bool> OnPasswordChecked = (sender, isCorrect) => { };
        #endregion

        #region Properties

        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private LocalProperties savedLocal;
        private GlobalProperties savedGlobal;

        public LocalProperties Local
        {
            get => (LocalProperties)Resources["localProperties"];
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                savedLocal = value.Clone();
            }
        }

        public GlobalProperties Global
        {
            get => (GlobalProperties)Resources["globalProperties"];
            set
            {
                if ((GlobalProperties)Resources["globalProperties"] == value)
                {
                    savedGlobal = value.Clone();
                    return;
                }
                ((GlobalProperties)Resources["globalProperties"]).Update(value);
                savedGlobal = value.Clone();
            }
        }

        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryGlobalNames();
            SetCategoryLocalNames();
        }
        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/DllCuirasaUProperties;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/DllCuirasaUProperties;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/DllCuirasaUProperties;component/Languages/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //TODO
            }

        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.Common.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
        }

        private void SetCategoryGlobalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyGlobal.Categories.Select(category => category.Name).ToList())
                    if(nameCategory != "Прочее")
                        PropertyGlobal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
            }
            catch (Exception)
            {

            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                    PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Access
        private bool _isAccessChecked = false;
        private readonly WindowPass _accessWindowPass = new WindowPass();


        private void InitAccessWindow()
        {
            _accessWindowPass.OnEnterInvalidPassword += _accessWindowPass_OnEnterInvalidPassword;
            _accessWindowPass.OnEnterValidPassword += _accessWindowPass_OnEnterValidPassword;
            _accessWindowPass.OnClosePasswordBox += _accessWindowPass_OnClosePasswordBox;
            _accessWindowPass.Resources = Resources;
            _accessWindowPass.SetResourceReference(WindowPass.LablePasswordProperty, "labelPassword");
        }

        private void _accessWindowPass_OnEnterValidPassword(object sender, EventArgs e)
        {
            _isAccessChecked = true;
            Local.Common.Access = AccessTypes.Admin;
            ChangeVisibilityLocalProperties(true);
            _accessWindowPass.Hide();
            OnPasswordChecked(this, true);
        }

        private void _accessWindowPass_OnClosePasswordBox(object sender, EventArgs e)
        {
            Local.Common.Access = AccessTypes.User;
        }

        private void _accessWindowPass_OnEnterInvalidPassword(object sender, EventArgs e)
        {
            Local.Common.Access = AccessTypes.User;
            _accessWindowPass.Hide();

            OnPasswordChecked(this, false);
        }

        private void CheckLocalAccess()
        {
            if (!_isAccessChecked)
            {
                Task.Run(() =>
                   Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                   {
                       Local.Common.Access = AccessTypes.User;
                       _accessWindowPass.ShowDialog();
                   }));
            }
            else
                _isAccessChecked = false;
        }

        public void SetPassword(string password)
        {
            _accessWindowPass.ValidPassword = password;
        }

        public void ChangeVisibilityLocalProperties(bool browsable)
        {
            PropertyLocal.Properties[nameof(LocalProperties.DB)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.DF)].IsBrowsable = browsable;
        }

        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            if (IsLocalSelected)
            {
                funcValidation(Local);
            }
            else
            {
                funcValidation(Global);
            }
        }


        #endregion

        public ControlProperties()
        {
            InitializeComponent();
            InitLocalProperties();
            InitGlobalProperties();
            ChangeLanguage(savedLocal.Common.Language);
            InitAccessWindow();
            
        }

        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            savedLocal.Common.PropertyChanged += CommonLocal_PropertyChanged;
            Local.Common.PropertyChanged += CheckLanguage_PropertyChanged;
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Common), typeof(LocalProperties), typeof(CommonLocal)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.DB), typeof(LocalProperties), typeof(EndPointConnection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.DF), typeof(LocalProperties), typeof(EndPointConnection)));
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
        }

        private void SetBindingAccess()
        {
            Binding binding = new Binding();
            binding.Source = Local.Common;
            binding.Path = new PropertyPath(nameof(Local.Common.Access));
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            BindingOperations.SetBinding(Global, GlobalProperties.AcssesProperty, binding);
        }

        private void InitGlobalProperties()
        {
            SetBindingAccess();
            savedGlobal = Global.Clone();
            Global.OnPropertyChanged += OnPropertyChanged;
            
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.RadioIntelegence), typeof(GlobalProperties), typeof(RadioIntelegence)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.Calibration), typeof(GlobalProperties), typeof(Calibration)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.CompassOrientation), typeof(GlobalProperties), typeof(Orientation)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.Location), typeof(GlobalProperties), typeof(Location)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.IQData), typeof(GlobalProperties), typeof(IQData)));
        }

        private void CheckLanguage_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Local.Common.Language):
                    ChangeLanguage(Local.Common.Language);
                    OnLanguageChanged(sender, Local.Common.Language);

                    break;
                case nameof(Local.Common.Access):

                    if (Local.Common.Access == AccessTypes.Admin)
                    {
                        CheckLocalAccess();
                    }
                    else
                    {
                        ChangeVisibilityLocalProperties(false);
                    }                 
                    break;
                default:
                    break;
            }
        }

        private void CommonLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
          
            switch (e.PropertyName)
            {
                case nameof(CommonLocal.Language):
                    ChangeLanguage(savedLocal.Common.Language);
                    OnLanguageChanged(sender, savedLocal.Common.Language);

                    break;
                
                default:
                    break;
            }
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                if (!savedLocal.EqualTo(Local))
                {
                    savedLocal.Update(Local.Clone());
                    OnLocalPropertiesChanged(this, savedLocal.Clone());
                }
            }
            else
            {
                if (!savedGlobal.EqualTo(Global))
                {
                    savedGlobal.Update(Global.Clone());
                    OnGlobalPropertiesChanged(this, savedGlobal.Clone());
                }
            }
        }

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                if (!savedLocal.EqualTo(Local))
                {
                    Local.Update(savedLocal.Clone());
                }
            }
            else
            {
                if (!savedGlobal.EqualTo(Global))
                {
                    Global.Update(savedGlobal.Clone());
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Validate();
        }

        private void ButDefault_Click(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                OnLocalDefaultButtonClick(this, null);
            }
            else
            {
                OnGlobalDefaultButtonClick(this, null);
            }
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
          ChangeVisibilityLocalProperties(false);  
        }
    }
}
