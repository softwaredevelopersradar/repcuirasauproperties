﻿using System.Windows.Controls;

namespace DllCuirasaUProperties
{
    public class EditorGetFolderCommands
    {
        #region Commands

        private static RelayCommand executeCommand;

        public static RelayCommand ExecuteCommand
        {
            get
            {
                return executeCommand ??
                    (executeCommand = new RelayCommand(ShowDialog, CanExecute));
            }
            set
            {
                if (executeCommand == value) return;
                executeCommand = value;
            }
        }


        public static void ShowDialog(object e)
        {
            System.Windows.Forms.FolderBrowserDialog ofd = new System.Windows.Forms.FolderBrowserDialog();

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                (e as TextBox).Text = ofd.SelectedPath;
                return;
            }

        }

        public static bool CanExecute(object e)
        {
            return true;
        }
        #endregion
    }
}
