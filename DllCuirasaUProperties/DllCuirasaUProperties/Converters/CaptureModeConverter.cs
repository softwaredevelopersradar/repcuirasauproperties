﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DllCuirasaUProperties.Converters
{
    public class CaptureModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Models.CaptureMode)System.Convert.ToByte(value);
        }
    }
}
