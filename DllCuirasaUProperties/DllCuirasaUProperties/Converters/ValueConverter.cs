﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DllCuirasaUProperties.Converters
{
    public class ValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = System.Convert.ChangeType(value, targetType);
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var result = System.Convert.ChangeType(value, targetType);
                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}
