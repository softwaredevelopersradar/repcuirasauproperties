﻿namespace DllCuirasaUProperties.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class DeviceAnalysisTypesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Models.DeviceAnalysisTypes)System.Convert.ToByte(value);
        }
    }
}