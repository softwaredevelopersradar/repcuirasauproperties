﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DllCuirasaUProperties.Converters { 
    public class AdminVisibleOnlyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Models.AccessTypes)value == Models.AccessTypes.Admin)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
