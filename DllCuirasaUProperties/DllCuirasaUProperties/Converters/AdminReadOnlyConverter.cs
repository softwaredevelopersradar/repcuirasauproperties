﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DllCuirasaUProperties.Converters
{
    public class AdminReadOnlyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Models.AccessTypes)value == Models.AccessTypes.Admin)
                return false;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == false)
                return Models.AccessTypes.Admin;
            return Models.AccessTypes.User;
        }
    }
}
