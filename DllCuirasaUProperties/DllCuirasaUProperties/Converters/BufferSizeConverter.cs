﻿using DllCuirasaUProperties.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace DllCuirasaUProperties.Converters
{
    public class BufferSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new BufferSizeList()[(byte)value];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var temp = (Models.BufferSizes)(byte)(new BufferSizeList().IndexOf((string)value));
            return temp;
        }
    }
}
