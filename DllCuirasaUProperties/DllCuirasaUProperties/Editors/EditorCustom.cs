﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using DllCuirasaUProperties.Models.Global;
using DllCuirasaUProperties.Models.Local;

namespace DllCuirasaUProperties.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(EndPointConnection), "EndPointEditorKey"},
            { typeof(CommonLocal), "CommonLocalEditorKey"},
            { typeof(RadioIntelegence), "RadioIntelegenceEditorKey" },
            { typeof(Location), "LocationEditorKey" },
            { typeof(Calibration), "CalibrationEditorKey" },
            { typeof(Orientation), "OrientationEditorKey" },
            { typeof(IQData), "IQDataEditorKey" }

        };

        public EditorCustom(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllCuirasaUProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }
    }
}
