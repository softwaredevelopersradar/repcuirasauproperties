﻿using System.ComponentModel;
using DllCuirasaUProperties.Models.Local;

namespace DllCuirasaUProperties.Models
{
    public class LocalProperties : IModelMethods<LocalProperties>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };
        public LocalProperties()
        {
            Common = new CommonLocal();
            DB = new EndPointConnection();
            DF = new EndPointConnection();

            Common.PropertyChanged += PropertyChanged;
            DB.PropertyChanged += PropertyChanged;
            DF.PropertyChanged += PropertyChanged;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public CommonLocal Common { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DB))]
        [DisplayName(" ")]
        public EndPointConnection DB { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DF))]
        [DisplayName(" ")]
        public EndPointConnection DF { get; set; }

        #region Model Methods

        public LocalProperties Clone()
        {
            return new LocalProperties
            {
                Common = Common.Clone(),
                DB = DB.Clone(),
                DF = DF.Clone()
            };
        }

        public void Update(LocalProperties localProperties)
        {
            Common.Update(localProperties.Common);
            DB.Update(localProperties.DB);
            DF.Update(localProperties.DF);
        }

        public bool EqualTo(LocalProperties localProperties)
        {
            return Common.EqualTo(localProperties.Common)
                && DB.EqualTo(localProperties.DB)
                && DF.EqualTo(localProperties.DF);
        }

        #endregion
    }
}
