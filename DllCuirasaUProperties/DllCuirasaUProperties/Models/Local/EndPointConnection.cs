﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllCuirasaUProperties.Models.Local
{
    public class EndPointConnection : AbstractBaseProperties<EndPointConnection>
    {
        #region Model Methods

        public override bool EqualTo(EndPointConnection model)
        {
            return IpAddress == model.IpAddress
                && Port == model.Port;
        }

        public override EndPointConnection Clone()
        {
            return new EndPointConnection
            {
                IpAddress = IpAddress,
                Port = Port
            };
        }

        public override void Update(EndPointConnection model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
        }

        #endregion

        private string _ipAddress = "";
        private int _port;

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => _ipAddress;
            set
            {
                if (_ipAddress == value) return;
                _ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => _port;
            set
            {
                if (_port == value) return;
                _port = value;
                OnPropertyChanged();
            }
        }

    }
}
