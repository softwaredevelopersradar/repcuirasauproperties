﻿namespace DllCuirasaUProperties.Models.Local
{

    public class CommonLocal : AbstractBaseProperties<CommonLocal>
    {
        #region IModelMethods

        public override bool EqualTo(CommonLocal model)
        {
            return Language == model.Language
                && AWS == model.AWS
                && Access == model.Access;
        }

        public override CommonLocal Clone()
        {
            return new CommonLocal
            {
                Language = Language,
                AWS = AWS,
                Access = Access
            };
        }

        public override void Update(CommonLocal model)
        {
            Language = model.Language;
            AWS = model.AWS;
            Access = model.Access;
        }

        #endregion
        
        private Languages _language;
        private byte _aws;
        private AccessTypes access = AccessTypes.User;
        public Languages Language
        {
            get => _language;
            set
            {
                if (_language == value) return;
                _language = value;
                OnPropertyChanged();
            }
        }

        public byte AWS
        {
            get => _aws;
            set
            {
                if (value == _aws) return;
                _aws = value;
                OnPropertyChanged();
            }
        }

        public AccessTypes Access
        {
            get => access;
            set
            {
                if (access == value) return;
                access = value;
                OnPropertyChanged();
            }
        }

    }
}
