﻿using System.Collections.ObjectModel;

namespace DllCuirasaUProperties.Models
{
    public class ReviewsNumberList : ObservableCollection<byte>
    {
        public ReviewsNumberList()
        {
            Add(4);
            Add(5);
            Add(6);
            Add(7);
            Add(8);
            Add(9);
            Add(10);
        }
    }
}
