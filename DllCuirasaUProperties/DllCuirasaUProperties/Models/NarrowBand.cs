﻿using System.Collections.ObjectModel;

namespace DllCuirasaUProperties.Models
{
    public class NarrowBand : ObservableCollection<byte>
    {
        public NarrowBand()
        {
            Add(10);
            Add(20);
            Add(30);
            Add(40);
            Add(50);
        }
    }
}
