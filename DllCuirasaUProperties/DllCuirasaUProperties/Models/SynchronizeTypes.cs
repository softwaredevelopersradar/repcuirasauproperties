﻿namespace DllCuirasaUProperties.Models
{
    public enum SynchronizeTypes : byte
    {
        Internal = 1,
        GPS = 2
    }
}
