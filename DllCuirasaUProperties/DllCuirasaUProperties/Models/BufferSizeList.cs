﻿using System.Collections.ObjectModel;

namespace DllCuirasaUProperties.Models
{
    public class BufferSizeList : ObservableCollection<string>
    {
        public BufferSizeList()
        {
            Add("1x");
            Add("2x");
            Add("4x");
            Add("8x");
            Add("16x");
            Add("32x");
            Add("64x");
            Add("128x");
            Add("256x");
        }
    }
}
