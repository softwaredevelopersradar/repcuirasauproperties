﻿using System.Collections.ObjectModel;

namespace DllCuirasaUProperties.Models
{
    public class AWSList : ObservableCollection<byte>
    {
        public AWSList()
        {
            Add(1);
            Add(2);
            Add(3);
            Add(4);
            Add(5);
            Add(6);
            Add(7);
            Add(8);
            Add(9);
            Add(10);
        }
    }
}
