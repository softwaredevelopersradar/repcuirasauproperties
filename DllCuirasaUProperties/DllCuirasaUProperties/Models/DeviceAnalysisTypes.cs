﻿namespace DllCuirasaUProperties.Models
{
    public enum DeviceAnalysisTypes : byte
    {
        Master,
        Slave,
        Both
    }
}