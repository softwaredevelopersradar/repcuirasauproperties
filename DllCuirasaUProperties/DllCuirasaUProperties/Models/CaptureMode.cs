﻿namespace DllCuirasaUProperties.Models
{
    public enum CaptureMode : byte
    {
        Single,
        Auto
    }
}
