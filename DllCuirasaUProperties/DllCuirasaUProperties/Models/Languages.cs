﻿using System.ComponentModel;

namespace DllCuirasaUProperties.Models
{
    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN
    }
}
