﻿using System.ComponentModel.DataAnnotations;

namespace DllCuirasaUProperties.Models.Global
{
    public class Location : AbstractBaseProperties<Location>
    {
        #region Model Methods

        public override bool EqualTo(Location model)
        {
            return Longitude == model.Longitude
                && Latitude == model.Latitude
                && Altitude == model.Altitude;
        }

        public override Location Clone()
        {
            return new Location
            {
                Longitude = Longitude,
                Latitude = Latitude,
                Altitude = Altitude,
            };
        }

        public override void Update(Location model)
        {
            Longitude = model.Longitude;
            Latitude = model.Latitude;
            Altitude = model.Altitude;
        }

        #endregion

        private float _latitude;
        private float _longitude;
        private float _altitude;

        [Range(0, 359)]
        public float Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();

            }
        }

        [Range(0, 359)]
        public float Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        public float Altitude
        {
            get => _altitude;
            set
            {
                if (_altitude == value) return;
                _altitude = value;
                OnPropertyChanged();
            }
        }

    }
}
