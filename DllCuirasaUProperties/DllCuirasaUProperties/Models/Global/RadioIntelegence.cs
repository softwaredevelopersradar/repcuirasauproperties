﻿using System.ComponentModel.DataAnnotations;

namespace DllCuirasaUProperties.Models.Global
{
    public class RadioIntelegence : AbstractBaseProperties<RadioIntelegence>
    {
        #region IModelMethods

        public override RadioIntelegence Clone()
        {
            return new RadioIntelegence
            {
                CourseAngle = CourseAngle,
                DurationMin = DurationMin,
                DurationMax = DurationMax,
                PeriodMin = PeriodMin,
                PeriodMax = PeriodMax,
                SynchronizeType = SynchronizeType,
                NumberOfReviews = NumberOfReviews,
                SwitchingTime = SwitchingTime,
                DeviceAnalysisType = DeviceAnalysisType,
                AutoCourseAngle = AutoCourseAngle
            };
        }

        public override bool EqualTo(RadioIntelegence model)
        {
            return CourseAngle == model.CourseAngle
                && DurationMin == model.DurationMin
                && DurationMax == model.DurationMax
                && PeriodMin == model.PeriodMin
                && PeriodMax == model.PeriodMax
                && SynchronizeType == model.SynchronizeType
                && SwitchingTime == model.SwitchingTime
                && NumberOfReviews == model.NumberOfReviews
                && DeviceAnalysisType == model.DeviceAnalysisType
                && AutoCourseAngle == model.AutoCourseAngle;
        }

        public override void Update(RadioIntelegence model)
        {
            CourseAngle = model.CourseAngle;
            DurationMin = model.DurationMin;
            DurationMax = model.DurationMax;
            PeriodMin = model.PeriodMin;
            PeriodMax = model.PeriodMax;
            SynchronizeType = model.SynchronizeType;
            SwitchingTime = model.SwitchingTime;
            NumberOfReviews = model.NumberOfReviews;
            DeviceAnalysisType = model.DeviceAnalysisType;
            AutoCourseAngle = model.AutoCourseAngle;
        }
        #endregion

        private bool _autoCourseAngle = true;
        private float _courseAngle;
        private SynchronizeTypes _synchronizeType;
        private float _durationMin = 2000;
        private float _durationMax = 2000;
        private float _periodMin = 10000;
        private float _periodMax = 10000;
        private short _switchingTime = 100;
        private short _numberOfReviews = 10;
        private float _narrowBand = 5;
        private DeviceAnalysisTypes deviceAnalysisType;


        public bool AutoCourseAngle
        {
            get => _autoCourseAngle;
            set
            {
                if(_autoCourseAngle == value) return;
                _autoCourseAngle = value;
                OnPropertyChanged();
            }
        }

        [Range(0,359)]
        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle == value) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }

        public SynchronizeTypes SynchronizeType
        {
            get => _synchronizeType;
            set
            {
                if (_synchronizeType == value) return;
                _synchronizeType = value;
                OnPropertyChanged();
            }
        }

        [Range(100,5000)]
        public float DurationMin
        {
            get => _durationMin;
            set
            {
                if (_durationMin == value) return;
                _durationMin = value;
                OnPropertyChanged();

            }
        }

        public float DurationMax
        {
            get => _durationMax;
            set
            {
                if (_durationMax == value) return;
                _durationMax = value;
                OnPropertyChanged();

            }
        }

        [Range(1000, 50000)]
        public float PeriodMin
        {
            get => _periodMin;
            set
            {
                if (_periodMin == value) return;
                _periodMin = value;
                OnPropertyChanged();
            }
        }


        public float PeriodMax
        {
            get => _periodMax;
            set
            {
                if (_periodMax == value) return;
                _periodMax = value;
                OnPropertyChanged();
            }
        }

        [Range(100, 1000)]
        public short SwitchingTime
        {
            get => _switchingTime;
            set
            {
                if (_switchingTime == value) return;
                _switchingTime = value;
                OnPropertyChanged();
            }

        }

        [Range(4, 10)]
        public short NumberOfReviews
        {
            get => _numberOfReviews;
            set
            {
                if (_numberOfReviews == value) return;
                _numberOfReviews = value;
                OnPropertyChanged();
            }
        }

        public float NarrowBand
        {
            get => _narrowBand;
            set
            {
                if (_narrowBand == value) return;
                _narrowBand = value;
                OnPropertyChanged();
            }
        }

        public DeviceAnalysisTypes DeviceAnalysisType
        {
            get => this.deviceAnalysisType;
            set
            {
                if (this.deviceAnalysisType == value)
                {
                    return;
                }

                this.deviceAnalysisType = value;
                this.OnPropertyChanged();
            }
        }
    }
}
