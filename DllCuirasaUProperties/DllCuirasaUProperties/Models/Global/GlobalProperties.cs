﻿using DllCuirasaUProperties.Models.Global;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using YamlDotNet.Serialization;

namespace DllCuirasaUProperties.Models
{
    [CategoryOrder(nameof(Calibration), 1)]
    [CategoryOrder(nameof(RadioIntelegence), 2)]
    [CategoryOrder(nameof(Location), 3)]
    [CategoryOrder(nameof(CompassOrientation), 4)]
    [CategoryOrder(nameof(IQData), 5)]
    public class GlobalProperties : DependencyObject, IModelMethods<GlobalProperties>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };
        public GlobalProperties()
        {
            RadioIntelegence = new RadioIntelegence();
            Calibration = new Calibration();
            Location = new Location();
            CompassOrientation = new Orientation();
            IQData = new IQData();
            RadioIntelegence.PropertyChanged += PropertyChanged;
            Calibration.PropertyChanged += PropertyChanged;
            Location.PropertyChanged += PropertyChanged;
            IQData.PropertyChanged += PropertyChanged;
            CompassOrientation.PropertyChanged += PropertyChanged;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }
       
        [Category(nameof(RadioIntelegence))]
        [DisplayName(" ")]
        public RadioIntelegence RadioIntelegence { get; set; }

        [Category(nameof(Location))]
        [DisplayName(" ")]
        public Location Location { get; set; }

        [Category(nameof(CompassOrientation))]
        [DisplayName(" ")]
        public Orientation CompassOrientation { get; set; }

        [Category(nameof(Calibration))]
        [DisplayName(" ")]
        public Calibration Calibration { get; set; }

        [Category(nameof(IQData))]
        [DisplayName(" ")]
        public IQData IQData { get; set; }


        #region IModelMethods

        public GlobalProperties Clone()
        {
            return new GlobalProperties
            {
                RadioIntelegence = RadioIntelegence.Clone(),
                Calibration = Calibration.Clone(),
                Location = Location.Clone(),
                CompassOrientation = CompassOrientation.Clone(),
                IQData = IQData.Clone()
            };
        }

        public bool EqualTo(GlobalProperties model)
        {
            return RadioIntelegence.EqualTo(model.RadioIntelegence)
                && Calibration.EqualTo(model.Calibration)
                && Location.EqualTo(model.Location)
                && CompassOrientation.EqualTo(model.CompassOrientation)
                && IQData.EqualTo(model.IQData);
        }

        public void Update(GlobalProperties model)
        {
            RadioIntelegence.Update(model.RadioIntelegence);
            Calibration.Update(model.Calibration);
            Location.Update(model.Location);
            CompassOrientation.Update(model.CompassOrientation);
            IQData.Update(model.IQData);
        }

        #endregion

        public static readonly DependencyProperty AcssesProperty = DependencyProperty.Register(nameof(Access), typeof(AccessTypes), typeof(GlobalProperties), new PropertyMetadata(AccessTypes.Admin, new PropertyChangedCallback(AccessChanged)));

        static void AccessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GlobalProperties control = (GlobalProperties)d;
            control.Access = (AccessTypes)e.NewValue;
        }

        [YamlIgnore]
        [Browsable(false)]
        public AccessTypes Access
        {
            get { return (AccessTypes)GetValue(AcssesProperty); }
            set { SetValue(AcssesProperty, value); }
        }

    }
}
