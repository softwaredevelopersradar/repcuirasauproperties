﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllCuirasaUProperties.Models.Global
{
    public class Orientation : AbstractBaseProperties<Orientation>
    {
        #region Model Methods

        public override bool EqualTo(Orientation model)
        {
            return Head == model.Head
                && Roll == model.Roll
                && Pitch == model.Pitch;
        }

        public override Orientation Clone()
        {
            return new Orientation
            {
                Roll = Roll,
                Pitch = Pitch,
                Head = Head
            };
        }

        public override void Update(Orientation model)
        {
            Roll = model.Roll;
            Pitch = model.Pitch;
            Head = model.Head;
        }

        #endregion

        private float _head;
        private float _roll;
        private float _pitch;

   

        [Range(0, 359)]
        public float Head
        {
            get => _head;
            set
            {
                if (_head == value) return;
                _head = value;
                OnPropertyChanged();
            }
        }

        [Range(-180, 180)]
        public float Roll
        {
            get => _roll;
            set
            {
                if (_roll == value) return;
                _roll = value;
                OnPropertyChanged();
            }
        }

        [Range(-85, 85)]
        public float Pitch
        {
            get => _pitch;
            set
            {
                if (_pitch == value) return;
                _pitch = value;
                OnPropertyChanged();
            }
        }

    }
}