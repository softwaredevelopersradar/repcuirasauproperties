﻿namespace DllCuirasaUProperties.Models.Global
{
    public class Calibration : AbstractBaseProperties<Calibration>
    {
        #region Model Methods

        public override bool EqualTo(Calibration model)
        {
            return Type == model.Type;
        }

        public override Calibration Clone()
        {
            return new Calibration
            {
                Type = Type
            };
        }

        public override void Update(Calibration model)
        {
            Type = model.Type;
        }

        #endregion

        private CalibrationTypes _type;

        public CalibrationTypes Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }
    }
}
