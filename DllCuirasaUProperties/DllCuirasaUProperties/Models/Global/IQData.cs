﻿using System.ComponentModel.DataAnnotations;

namespace DllCuirasaUProperties.Models.Global
{
    public class IQData : AbstractBaseProperties<IQData>
    {
        #region IModelMethods
        public override IQData Clone()
        {
            return new IQData
            {
                TimeRecord = TimeRecord,
                PathFolderNbFiles = PathFolderNbFiles,
                Recorder = Recorder,
                CaptureMode = CaptureMode,
                InputBuffer = InputBuffer,
                OutputBuffer = OutputBuffer,
                WaitForStrobe = WaitForStrobe
        };
        }

        public override bool EqualTo(IQData model)
        {
            return Recorder == model.Recorder
                && CaptureMode == model.CaptureMode
                && InputBuffer == model.InputBuffer
                && OutputBuffer == model.OutputBuffer
                && WaitForStrobe == model.WaitForStrobe
                && TimeRecord == model.TimeRecord
                && PathFolderNbFiles == model.PathFolderNbFiles;
        }

        public override void Update(IQData model)
        {
            Recorder = model.Recorder;
            CaptureMode = model.CaptureMode;
            InputBuffer = model.InputBuffer;
            OutputBuffer = model.OutputBuffer;
            WaitForStrobe = model.WaitForStrobe;
            TimeRecord = model.TimeRecord;
            PathFolderNbFiles = model.PathFolderNbFiles;
        }
        #endregion


        private float _timeRecord = 8;
        private string _pathFolderNbFiles;
        private Recorder _recorder = Recorder.Master;
        private CaptureMode _captureMode = CaptureMode.Auto;
        private BufferSizes _inputBuffer = BufferSizes.x32;
        private BufferSizes _outputBuffer = BufferSizes.x32;
        private bool _waitForStrobe = true;

        [Range(0.1, 10)]
        public float TimeRecord
        {
            get => _timeRecord;
            set
            {
                if (_timeRecord == value) return;
                _timeRecord = value;
                OnPropertyChanged();
            }
        }

        public string PathFolderNbFiles
        {
            get => _pathFolderNbFiles;
            set
            {
                if (_pathFolderNbFiles == value) return;
                _pathFolderNbFiles = value;
                OnPropertyChanged();
            }
        }


        public Recorder Recorder
        {
            get => _recorder;
            set
            {
                if (_recorder == value) return;
                _recorder = value;
                OnPropertyChanged();
            }
        }


        public CaptureMode CaptureMode
        {
            get => _captureMode;
            set
            {
                if (_captureMode == value) return;
                _captureMode = value;
                OnPropertyChanged();
            }
        }


        public BufferSizes InputBuffer
        {
            get => _inputBuffer;
            set
            {
                if (_inputBuffer == value) return;
                _inputBuffer = value;
                OnPropertyChanged();
            }
        }

        public BufferSizes OutputBuffer
        {
            get => _outputBuffer;
            set
            {
                if (_outputBuffer == value) return;
                _outputBuffer = value;
                OnPropertyChanged();
            }
        }


        public bool WaitForStrobe
        {
            get => _waitForStrobe;
            set
            {
                if (_waitForStrobe == value) return;
                _waitForStrobe = value;
                OnPropertyChanged();
            }
        }
    }
}
