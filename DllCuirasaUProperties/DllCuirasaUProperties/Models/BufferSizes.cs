﻿namespace DllCuirasaUProperties.Models
{
    public enum BufferSizes : byte
    {
        x1,
        x2,
        x4,
        x8,
        x16,
        x32,
        x64,
        x128,
        x256
    }
}
